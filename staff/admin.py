from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from .models import *

admin.site.site_header = 'ptam人员管理平台'
admin.site.site_title = 'ptam人员管理平台'

admin.site.register(PartnerCompany)
admin.site.register(Tl)
admin.site.register(Tam)
admin.site.register(Location)


class TlFilter(SimpleListFilter):
    title = 'TL'
    parameter_name = 'tl'

    def lookups(self, request, model_admin):
        return [(c.id, c.nickname) for c in Tl.objects.all()]

    def queryset(self, request, queryset):
        if not self.value():
            return queryset
        return queryset.filter(tam__tl__id__exact=self.value())


class CityFilter(SimpleListFilter):
    title = '城市'
    parameter_name = 'city'

    def lookups(self, request, model_admin):
        citys = set([location.city for location in Location.objects.all()])
        return [(city, city) for city in citys]

    def queryset(self, request, queryset):
        if not self.value():
            return queryset
        return queryset.filter(location__city__exact=self.value())


@admin.register(Ptam)
class PtamAdmin(admin.ModelAdmin):
    list_display = ('username', 'company', 'tam', 'tl', 'city', 'entry_time', 'status')
    list_filter = (CityFilter, TlFilter, 'status', 'company')
    search_fields = ('tam', 'username')
    ordering = ('-entry_time',)
