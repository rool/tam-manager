from django.db import models
from django.utils import timezone


class Tl(models.Model):
    username = models.CharField(max_length=30, verbose_name="姓名")
    nickname = models.CharField(max_length=30, verbose_name="花名", unique=True)

    def __str__(self):
        return self.nickname

    class Meta:
        verbose_name = "TL"
        verbose_name_plural = verbose_name


class Tam(models.Model):
    username = models.CharField(max_length=30, verbose_name="姓名")
    nickname = models.CharField(max_length=30, verbose_name="花名", unique=True)
    tl = models.ForeignKey(Tl, on_delete=models.PROTECT)

    def __str__(self):
        return self.username+'('+self.nickname+')'

    class Meta:
        verbose_name = "TAM"
        verbose_name_plural = verbose_name


class PartnerCompany(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "合作方"
        verbose_name_plural = verbose_name


class Location(models.Model):
    city = models.CharField(max_length=10)
    name = models.CharField(max_length=30)
    address = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return '('+self.city+')'+self.name

    class Meta:
        unique_together = ('city', 'name')
        verbose_name = "工作地点"
        verbose_name_plural = verbose_name


class Ptam(models.Model):
    username = models.CharField(max_length=30, verbose_name="姓名")
    company = models.ForeignKey(PartnerCompany, on_delete=models.PROTECT, verbose_name='单位')
    tam = models.ForeignKey(Tam, on_delete=models.PROTECT)
    location = models.ForeignKey(Location, on_delete=models.PROTECT)
    entry_time = models.DateTimeField(default=timezone.now, verbose_name='入职时间')
    STATUS_CHOICES = [
        ('training', '岗前培训'),
        ('graduated', '已毕业'),
    ]
    status = models.CharField('状态', choices=STATUS_CHOICES, max_length=20, default='graduated')

    @property
    def tl(self):
        return self.tam.tl

    @property
    def city(self):
        return self.location.city

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = "PTAM"
        verbose_name_plural = verbose_name
